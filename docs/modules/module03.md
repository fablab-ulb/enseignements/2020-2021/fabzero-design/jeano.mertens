# 3. Impression 3D

Après avoir reproduit mon objet sur **Fusion 360**, j'ai exporté le fichier en .stl afin de pouvoir l'importer sur [PrusaSlicer](https://www.prusa3d.fr/prusaslicer/).

![prusa](../images/prusa1.png)

# PrusaSlicer

Une fois importé, il faut positionner l'objet pour que l'impression se passe dans une façon la plus optimale que possible. J'ai du donc pivoté mon objet pour que la face plane de la table basse soit positionnée vers le bas. 

![prusa](../images/prusa2.png)

Dans cette position, le besoin d'un support n'est pas nécessaire. Sinon, il faut aller dans "réglages d'impression" -> "Support" -> cochez la case "Générer des supports", comme montré sur la photo ci-dessous.

Nous pouvons aussi changer quelques paramètres tels que: 
- la hauteur de couche 
- le remplissage: on peut choisir la densité de remplissage, le motif de remplissage qui donne la rigidité de l'objet, etc.
- la jupe et borduge: cela permet à l'objet de ne pas bouger durant l'impression.
- les supports: cela permet de rajouter des supports/structures à l'objet qui possède des parties "flottantes".
- la vitesse d'impression
- etc. 

![prusa paramètres](../images/prusacouche.png)
![prusa paramètres](../images/prusaremplissage.png)

Une fois que les paramètrages vous conviennent, il faut exporter le fichier et le mettre dans une carte SD (nos imprimantes 3D fonctionnent qu'avec des cartes SD. Il existe biensûr des machines qui possède un port USB) et la mettre dans votre imprimante 3D.

![impirmante 3D](../images/prusa_machine.png)

Une fois que la carte SD est mise dans la machine, il faut choisir le fichier qui contient votre création 3D et vous pouvez lancer l'impression. Vous pouvez aussi modifier la vitesse d'impression mais attention, une vitesse très rapide peut rater l'impression. Avant de lancer l'impression, il ne faut pas oublier de nettoyer la plateforme avec un chiffon et du dissolvant si nécessaire.
Enfin, il faut attendre que la machine atteigne sa température avant qu'elle puisse lancer l'impression et c'est conseillé aussi d'assister quelques minutes le début de l'impression au cas où l'on rencontre un problème.

# Objet imprimé

![kappa vues](../images/kappastrip1.png)
![kappa vues](../images/kappastrip2.png)

Comme on peut voir, l'objet s'est bien imprimé mais j'ai remarqué qu'il y avait un défaut d'impression. Il y a deux fentes qui sont apparus.
Je suis donc retourné sur mon fichier Fusion 360 et j'ai remarqué qu'il y avait un problème de balayage. La forme du départ et la forme d'arrivée ne se liaient pas correctement et cela a engendré une partie très fine que le programme Prusa a détecté comme "vide".
J'ai alors remodélisé l'objet sur **Fusion 360**, comme vous l'auriez vu dans la partie [Module 2](https://fablab-ulb.gitlab.io/enseignements/2020-2021/fabzero-design/jeano.mertens/modules/module02/#re-modelisation-de-lobjet).

# Objet réimprimé

Après la modélisation, j'ai réimprimé le nouveau modèle de mon objet avec une autre couleur cette fois-ci.

![objet réimprimé en jaune](images/kappastrip3.png)
