# 2. Modélisation 3D

## Introduction à Fusion 360

_Fusion 360 est un logiciel convivial et polyvalent, qui autorise une intégration des processus de la conception, depuis le design et la simulation, jusqu'à la fabrication._ 

Comme le logiciel Sketchup que nous, les étudiants en architecture, connaissons, c'est un logiciel qui permet de faire des modélisation 3D et faire des "render". Avec la license étudiante, vous pouvez installer **Fusion 360** gratuitement dans [ce lien](https://www.autodesk.com/campaigns/education/student-design)!

Voici les différentes opérations et fonctions que l'on peut trouver sur l'interface:
![fusion 360 barre d'opération](docs/images/fusioninterface.png)

# Modélisation de l'objet

![étapes](../images/doc1.png) 

* J'ai décidé de travailler sur la moitié de l'objet étant donné que celui-ci est symétrique. Il suffira donc de **dupliquer** l'objet et de faire une symétrie après.
* Tout d'abord, j'ai tracé un rectangle avec le profil du pli. ![créer](../images/fusioncreate.png)
* Puis, j'ai utilisé l'opération **extruder** pour faire un parallélipipède. ![extruder](../images/fusionextruder.png)

![étapes](../images/doc2.png)

* J'ai ensuite tracé le chemin sur la face "haut" du parallélipipède que le profil du pli va suivre avec l'opération **balayage**.
![balayage](docs/images/fusionbalayage.png)
* Après cela, j'ai tracé les deux profils sur les faces "gauche" et "droite". C'est deux profils serviront pour l'opération **lissage**. Lors de cette opération, je me suis confronté à un problème. Il y avait des faces qui n'étaient pas lisses ou droite. J'ai du donc retracer des planes et utiliser **extruder** pour lisser certaines parties de l'objet.
![lissage](docs/images/fusionlissage.png)

![étape finale](../images/doc3.png)

* Après le problème résolu, j'ai enfin dupliqué l.'objet et faire la symétrie. Comme je ne savais pas comment il faut faire, j'ai cherché sur [_YouTube_ un tutoriel](https://www.youtube.com/watch?v=IM_sxhp0jzk) qui montre cela.

# Re-modélisation de l'objet

Lorsque j'ai imprimé mon objet que vous pouvez voir sur la partie Module 3, il y a eu un défaut d'impression. Deux trous se sont créés lorsque mon objet s'est imprimé. Aussi, la première modélisation n'était pas trop détaillée.

![objet kappa](../images/kappastrip2.png)
![kappa](../images/kappastrip1.png)

J'ai alors essayé de re-modéliser sur **Fusion 360** avec plus de détails! _Il faut lire les images de haut en bas._

![step1](../images/doc4.png)
![step1](../images/doc5.png)
![step1](../images/doc6.png)
![step1](../images/doc7.png)

Petite remarque: lorsque j'effectue l'opération de **balayage** ou **lissage**, le dessin du profil disparaît. J'ai du alors redessiner ou plutôt copier-coller le dessin du profil après chaque opération.
Pour l'opération de **lissage**, j'ai du aussi changer manuellement les points qui se relient pour éviter la même erreur précédemment.

# Système de filetage

Lors de la conception de mon projet pour le pré-jury, j'ai décidé d'utiliser un système de filetage et de boulon qui vont servir de lien des différentes pièces du projet. J'ai suivi [ce tutoriel](https://www.youtube.com/watch?v=pxneNgbq7Pk) trouvé sur _YouTube_. 

Il faut utiliser l'option "thread" sur la surface arrondi d'un cylindre, régler le diamètre et le type de filetage suite aux normes proposées.