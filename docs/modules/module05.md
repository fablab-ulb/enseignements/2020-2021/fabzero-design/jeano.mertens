# 5. Shaper Origin

Dans ce module, j'ai appris à utiliser la **Shaper** qui est une machine qui rend le fraisage de précision facile et accessible. Elle peut faire une découpe de 43mm max et peut faire également des gravures.

## Précaution d'usage

* Utiliser la machine sur un plan de travail stable
* Pour la découpe : fixer le matériau sur le plan de travail avec un tape double-face ou avec des visses
* Toujours relever la fraiseuse (bouton rouge) avant de passer d'une forme à une autre
* Ne pas oublier d'allumer l'aspirateur de poussière durant le fraisage

## Tutoriel

Pour un tutoriel plus précis, Shaper Tools dispose [des vidéos](https://www.shapertools.com/fr-fr/tutorials) sur leur site.

## Utilisation

* Préparer le plan de travail nécessaire en mettant plusieurs bandes de ShaperTape sur l'espace nécessaire, espacées d'environ 8cm. 
* Scanner le plan de travail.
* Mettre le fichier svg dans une clé USB et l'insérer dans le port USB à droite de la machine. Importer le fichier sur l'espace de travail.
* Changer les paramètres désirés. Dans mon cas, j'ai décidé de découper autour de la ligne, à une profondeur de 1,2 cm (découper en une trajet la planche) et avec une fraise de 8mm.
* Régler l'axe Z de la fraise lorsque l'on change de mèche.
* Pour changer la fraise, il faut d'abord débrancher le mandrin et l'enlever de son support. Appuyer sur la tige de verrouillage tout en desserrant ou en serrant l'écrou de verrouillage à l'aide de la clé de 19mm fournie. Serrer l'écrou lorsque la fraise est installée. Remettre le mandrin dans son support et le rebrancher.
* Lorsque le paramètrage est fini, vous pouvez allumer l'aspirateur et la Shaper.
* Pour commencer, visez le dessin dans le cercle. Ensuite, appuyez sur le bouton vert de la mannette droite pour commencer le fraisage et suivez le chemin indiqué par la petite flèche.
* Appuyer sur le bouton orange de la mannette gauche pour arrêter le fraisage et relever la fraise.

## Petite expérimentation

Avec la Shaper Origin, j'ai découpé le début d'une petite maquette d'un projet. Le but était de découper des lamelles et la Shaper Origin était la meilleure machine avec laquelle je pouvais faire des formes courbées. 

![shaper try](images/shapertry.png)
