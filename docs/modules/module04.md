# 4. Découpe assistée par ordinateur

# Connaissances des machines

Tout d'abord, il est important de savoir sur quelle machine on travail. Dans notre Fablab, on utilise le Lasersaur.

Matériaux recommandés:
- bois contreplaqué (plywood/multiplex)
- acrylique (PMMA/Plexiglass) ATTENTION à savoir la différence entre le plexiglass et le pvc!
- papier, carton
- textiles

Matériaux déconseillés:
- MDF (fumée épaisse et très nocive à longue terme)
- ABS/PS (fond facilement et fumée nocive)
- PE, PET, PP
- Composite à base de fibres
- Métaux

Matériaux interdits:
- PVC (fumée acide et nocive)
- Cuivre (réfléchit totalement le laser)
- Téflon (fumée acide et nocive)
- Vinyl, semili-cuir (peut contenir de la chlorine)

# Précaution à prendre

1. Connaître avec certitude quel matériau est découpé
2. Toujours ouvrir la vanne d'air comprimé
![vanne d'air comprimé](../images/vannedaircomprimé.png)
3. Toujour allumer l'extracteur de fumée.
4. Savoir où est le bouton d'arrêt d'urgence
![bouton d'arrêt d'urgence](../images/bouton.png)
5. Savoir où trouver un extincteur au CO2

# Énoncé de l'exercice

L'énoncé de l'exercice et de faire une lampe ou un abat jour en s'inspirant de notre objet choisi. Cette lampe serait faite en polypropylène découpé par la machine à découpe au laser. Pour ma part, mon objet est la **_Kappa_** de Cesare Leonardi & Franca Stagi.

# Premier essai

J'ai tout d'abord commencé par écrire des mots clés qui sont en lien avec mon objet. C'est une méthode de brainstorming qui m'a permis de nourrir mes idées. Après avoir fait ce petit exercice, j'ai décidé de commencer le projet de lampe à partir des mots **triangle-origami-plier-rangement-flotter-rigide**.

![brainstorming](../images/brainstorming_lamp.png)

Pour apprendre un peu plus sur les origamis, j'ai regardé des vidéos tutorielles sur _YouTube_ sur les différentes façons de faire des origamis avec la triangulation. Je suis tombé sur [cette vidéo](https://www.youtube.com/watch?v=zW9loM1ow9w) qui m'a inspiré le plus. J'ai alors essayé de reproduire le modèle et de l'explorer en le posant autrement, en modifiant les plis.

![origami](../images/origami.png)

Ensuite, j'ai eu l'idée de faire un deuxième modèle pour essayer de trouver un système modulaire. Voici quelques photos qui montrent les différentes façons d'assembler les deux modèles.

![origami](../images/origamia.png)

Après cela, j'ai essayé de trouver un _pattern_ d'origami qui va me permettre de le reproduire à partir d'**un seul** modèle. Étant donné qu'avec la feuille de polypropylène c'est impossible de faire de l'origami, j'ai essayé de trouver un _pattern_ qui évite les plis "compliqués". J'ai alors essayé ce premier pattern:

![premier essai](../images/origamipattern.png)

Malheureusement, je n'ai pas obtenu le résultat que je voulais. Il manquait une partie du _pattern_, ce qui rend l'origami peu stable. 

![premier essai 2](../images/test1.png)

# Deuxième essai

J'ai commencé à explorer différentes formes et différentes façons d'assembler des choses.

![expérience 1](../images/e5.png)

J'ai alors cherché quelquees références qui m'ont inspiré. Ces lampes évoque les termes: rigide, triangle et flotter.

![références de lampes](../images/e6.png)

Avant de se mettre dans l'expérimentation, voici le croquis qui explique comment j'ai voulu procéder.

![croquis](../images/croquis.png)

Les étapes:
    - J'ai commencé par découper une bandes de 4cm x 16cm avec deux entails de 2cm à 1cm du bout de la bande, comme montré sur le croquis. **Répéter cette étape neuf fois** et faire un 3 groupes de 3 bandes.
    - Avec les entails, relier les 3 bandes pour faire une formes triangulaire. **Répéter cette étape trois fois**. Nous obtenons alors trois triangles à partir des bandes.
    - Pour faire l'effet flottant, j'ai découpé 6 petits carrés de 4cm x 4cm avec quatre entails de 1cm, comme montré sur le croquis. Ces petits carrés vont s'insérer entre les triangles pour avoir l'impression qu'ils flottent.
    - Enfin, pour rajouter un support, qui est une fonction relative à mon objet de départ, j'ai découpé un triangle équialatéral de 18cm de côté. J'ai rajouté des éléments qui vont donner du poids à ce support: deux triangles et un grand rectangle, comme montré sur le croquis.

Voici ce que cela donnerait avec du papier.

![lampe en papier](../images/e4.png)
![lampe en papier dans le noir](../images/e7.png)

# Conception

Pour faire une découpe laser, il faut la faire à partir d'un dessin 2D vectoriel enregistré sous un fichier .SVG. Le logiciel de prédilection est [Inkscape](https://inkscape.org/fr/release/1.0.1/windows/64-bit/), car la DriveboardApp (programme sur qui est relié à la machine et qui lance la découpe) préfère les fichiers SVG qui sortent de Inkscape. La DriveboardApp peut lire aussi les documents .DXF mais il en résulte souvent des problèmes avec les couleurs et les groupements. 
Pour ma part, j'ai commencé à faire le dessin 2D sur AutoCad que j'ai ensuite exporté en un fichier .DXF.

![dessin autocad](../images/auto1.png)

Lorsque j'ai importé le fichier DXF sur Inkscape, ce message apparaît.
![error](../images/ink2.png)

J'ai alors essayé d'ouvrir le fichier .DXF sur Illustrator mais un message d'erreur est aussi apparu. J'ai donc décidé de refaire le dessin sur illustrator afin de pouvoir l'importer sur Inkscape pour vérifier les éventuels problèmes.
Une fois le dessin de la découpe fait sur Illustrator, je l'ai exporté en un fichier .SVG et je l'ai ensuite réussi de l'importer sur Inkscape. J'ai utilisé deux couleurs: le **noir** pour la **découpe** et le **rouge** pour la **gravure**. 
![inkscape](../images/ink1.png)

# Drive BoardApp

Maintenant, il faut mettre le fichier .SVG sur une clé USB qu'on vient connecter à l'ordinateur qui est relié à la Lasersaur. Il faut ouvrir le fichier avec l'application Drive BoardApp et changer les paramètres pour assurer une bonne découpe. Je commence par mettre la couleur de la gravure (noir) pour éviter que la feuille se déplace. Je mette la vitesse F à 1500 qui est le paramètre par défaut et la puissance de la gravure à 20%. Pour la découpe, je mets la couleur rouge, la vitesse F reste la même chose mais la puissance de la découpe sera à 46%. Ce sont les paramètres que mes collègues ont utilisé que j'ai suivi.

Une fois que le paramètrage est fini, je clique sur la maison pour remettre le laser à son point d'origine. On peut utiliser le bouton "Offset" pour régler la position du dessin si nécessaire.
Lorsque la machine est allumé et le bouton "Statut" passe au vert, on peut commencer la découpe.
