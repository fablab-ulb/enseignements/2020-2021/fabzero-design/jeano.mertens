# 1. GITLAB

_(Tout d'abord, je tiens à dire que j'étais perdu au début, mais grâce à mes collègues et à ma binôme Elodie Nivet, j'ai su rebondir directement sur l'installation de Git jusqu'à la documentation des modules.)_

Tout d'abord, c'est important d'avoir bash sur son ordinateur. Pour ma part, puisque je possède un Windows, j'ai du installer [Git Bash](https://gitforwindows.org/) pour pouvoir effectuer certaines opérations. 

Ensuite, j'ai du installer Git. Puisque je possède un Windows, je l'ai téléchargé à partir de ce [site web](https://git-scm.com/downloads). Ensuite, je l'ai configuré à l'aide du fichier [Documentation.md](https://docs.gitlab.com/ee/gitlab-basics/start-using-git.html#configure-git), sur le point **Configure Git**.

Git étant installé, je devais générer une clé SSH afin de connecter le serveur à mon PC local. Pour la généré, j'ai suivi les étapes montrées dans [**Creating your SSH key pair**](https://docs.gitlab.com/ee/gitlab-basics/create-your-ssh-keys.html) et [**Generating a new SSH key pair**](https://docs.gitlab.com/ee/ssh/README.html#generating-a-new-ssh-key-pair).

![clé ssh](docs/images/sshkey.png)

Une fois que j'ai téléchargé la clef SSH, je l'ai ouverte dans **Notes** (on peut l'ouvrir sur Atom aussi) pour pouvoir la copier et je suis venu l'insérer dans mon profil Git.

# Fichier local
J'ai ensuite cloné mon fichier "jeano.mertens" sur mon local. Vous pouvez trouver le lien SSH ici:
![profil ssh](docs/images/clonessh.png)

J'ai suivi [la procédure de clôner via SSH donnée(https://docs.gitlab.com/ee/gitlab-basics/start-using-git.html#clone-via-ssh), en copiant la clef SSH du fichier sur GitLab et en utilisant le code donné dans le tuto.

![ssh sur bash](docs/images/clonessh2.png)

Ensuite, j'ai suivi cette procédure pour pouvoir mettre à jour les dernières modifications faites.
![screenshot procédure](docs/images/synchro.png)

# Écrire en Markdown
Ensuite, voici quelques astuces pour modifier le blog en markdown.
 - Lier un mot à un lien. Ici, j'ai lié le mot "portfolio" au lien de mon site web créé l'année passée pour le cours d'architecture et représentation. Pour faire cela, il faut: [mot](lien internet)
 - Rajouter une photo. Tout d'abord, il faut charger la photo sur le dossier "images". Sur la barre gauche de GitLab, cliquez sur _Repository_ -> _docs_ -> _images_. Ensuite, pour charger la photo en markdown, il faut mettre un point d'exclamation suivi d'un mot descriptif entre [] pour les malvoyants et après le lien de la photo.
 - Changer la taille de l'image. Pour ne pas charger le stockage et ralentir le chargement, il faut réduire la taille de l'image. Après avoir installé [GraphicsMagick](http://www.graphicsmagick.org/), il faut ensuite ouvrir bash et mettre le code: 
 
 gm convert -resize 600x600 bigimage.png smallimage.jpg 

 - Créer une suite d'images. Pour faire cela rapidement, il faut mettre ce code sur bash:

 gm convert +append -geometry x400 image1.png image2.png image3.png image4.png strip.png


* **Premier exercice**
 - utiliser les ** et _ pour rendre des mots en gras ou en italique. Pour mettre un mot en gras, il faut mettre le mot entre ** ** et pour mettre les mots en italique, il faut mettre le mot entre _ _.

* **Documenter le blog**
 - l'utilisation de l'astérisque et ensuite des tirets pour faire la liste des modifications.


# Problème de photos qui ne s'affichent pas
Pendant la documentation, j'ai eu un petit problème concernant les photos. Avant, mes photos apparaissaient à l'aide du code markdown: ! + [petit mot ou petite phrase] + (images/photo.png). C'est un code que j'ai copié du template du blog.

Au fur et à mesure que je travaille sur mon blog, j'ai remarqué que mes photos ne re-apparaissaient plus. Elles étaient remplacées par une icône d'une image "cassée" + le nom du fichier de l'image. J'ai alors essayé d'upload une nouvelle image en espérant qu'elle s'affiche mais le problème persiste. 

Après quelques essais dans l'écriture markdown, il suffisait simplement d'écrire le code comme celui-ci:
! + [petit mot ou petite phrase] + (docs/images/photo.png)

# Update de photos qui ne s'affichent pas
Après avoir constaté les sites des autres étudiants, j'ai remarqué que plusieurs utilisent différent codes. Certains mettent "..." avant le "/images", d'autres mettent "docs/images" comme moi et d'autres étudiants mettent rien. À ce stade, je ne sais pas si c'est le site qui bug ou les codes ne fonctionnent pas.