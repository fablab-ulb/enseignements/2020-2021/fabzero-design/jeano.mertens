# Final Project

## Objet final - TAFL 

![design final](images/designfinal1.png)

Le nom **TAFL** signifie “TAble-FLottante” et vient d’un jeu de mot de “_tafel_” qui est la traduction néerlandaise de la table. C’est une table d’appoint qui suit les codes du minimalisme aux temps d’aujourd’hui. Elle s’inspire de la KAPPA de Cesare Leonardi & Franca Stagi. Pli, rangement, flotter, origami, ... voici les mots qui sont souvent apparus de mon esprit lors de mon brainstorming sur la KAPPA. Ces mots m’ont inspiré à sortir de l’ordinaire et à voir différemment la KAPPA, tout en respectant son identité pure et surtout le travail du dessin du profil très complexe mais en même temps très simple. La TAFL, à partir d’un fin dessin 2D, devient fonctionnelle par le geste de l’étirement en 3D. Le profil est le résultat du travail de plusieurs croquis de conception et de plusieurs petites maquettes. Avec **Fusion 360**, j'ai essayé plusieurs manipulations, par le geste de l'étirement, avec ce profil et cela m'a beaucoup aidé de percevoir l'objet en 3D avant de le réaliser à une échelle plus petite grâce à l'imprimante 3D. Enfin, j'ai réalisé plus tard que c'est plutôt la fonction qui génère la forme et non l'inverse. C'est la table qui a pris la forme du livre. Si un autre objet était ciblé, on obtiendrait un nouveau profil.

_La forme de l'objet final était trop ambitieuse pour réaliser à l'échelle réelle. J'étais confronté à faire des décisions difficiles dans le choix de matériaux et la manière de la concenvoir la table d'appoint, ce qui a aussi amené quelques erreurs de fabrication. L'objet final n'était pas ce à quoi je m'attendais, mais je suis très satisfait du long processus de conception._

![prototype à l'échelle réelle](images/prototype_final.png)

![exposition](images/table_de_présentation.png)


## Recherches

Avant de se lancer dans la conception du projet, on nous a demandé de regarder et comprendre attentivement les défintions de ces **cinq** mots et de le documenter. Cela va nous servir de guide et va nous permettre de comprendre notre processus de pensée:

  - **Référence**
  - **Influence**
  - **Inspiration**
  - **Hommage**
  - **Extension**

On nous a fourni **quatre** dictionnaires d'approches et d'époques différentes:

  - **Wiktionnaire** est contemporain, en perpétuelle évolution et collaboratif.
  - **Le petit Robert** à été créé à l'époque de la collection du musée.(étymologique)
  - **Le petit Larousse** est un classique, largement diffusé, ouvrage français.(encyclopédique)
  - **Le Littré** date de la fin du 19 ème siècle au début de l'industrialisation, plutôt littéraire.

Ensuite, parmis ces **cinq** mots, il faut choisir un ou deux que nous allons interpréter et qui vont nous guider dans la conception du projet.

**Choix des mots**

Après plusieurs lectures et comparaison des différentes définitions, j'ai décidé de choisir les mots **Inspiration** et **Extension**, et j'ai décidé de réécrire les définitions qui m'ont les plus évoqué.

**Inspiration**: 
  - acte de stimulation de l’intellect, des émotions et de la créativité à partir d’une influence (Wiktionnary)
  - action d'inspirer, de conseiller quelque chose à quelqu'un (Le Petit Robert)
  - influence exercée sur un auteur, sur une oeuvre (Larousse)
  - L'enthousiasme qui entraîne les poëtes, les musiciens, les peintres (Littré)

Pour le mot **Inspiration**, j'ai remarqué que ce mot était en lien avec le mot **Influence**. J'ai interprété de manière banal, avec mes propres mots, le lien de ces deux mots par cet exemple: 

  "Une personne exerçant une action est l'influenceur et a personne qui est affectée par cette action est influencée OU a choisi de s'inspirer de ce que la première ait fait. Tout dépend du point de vue."

J'ai donc décidé de m'inspirer pas seulement de l'objet Kappa mais aussi des autres créations de Cesare Leonardi & Franca Stagi, que ce soit les meubles ou d'autres projets. 
![créations de cesare leonardi franca stagi](images/cl_fs_creations.png)

Avec les différentes définitions, j'aimerais évoquer une émotion/un sentiment à travers l'objet: la tristessse, la joie, la colère, la timidité, l'anxiété, la passion, etc. J'ai été inspiré par la KAPPA par le dessin du profil et le jeu des formes à la fois complexe et simple.


**Atlas of Emotions**

En cherchant plus loin sur l'idée de transmettre une émotion à travers d'un objet, je suis tombé sur un article **[The Shapes of Emotions](https://hi.stamen.com/the-shapes-of-emotions-72c3851143e2)** qui illustre les émotions (colère, peur, dégoût, tristesse, and jouissance) avec des formes:

![figures et émotions](images/émotionsfigures.png)
[source](https://hi.stamen.com/the-shapes-of-emotions-72c3851143e2)

Pour les cinq émotions, chaqe forme est simplement une variation d'un triangle dont les coins marquent trois points significatifs. Les coins gauche et droite de la base du triangle montre le minimum et le maximum de l'intensité de l'émotion en question, alors que la hauteur marque l'intensité en moyenne.
Prenons comme exemple la **colère**. L'agacement et la fureur sont des états de colère, mais chaque état varie par son intensité. L'agacement est relativement léger et la fureur est toujours intense. Les états et les couleurs de chaque émotion sont illustrés sur la photo ci-dessus. En général, on associe la forme triangulaire et la couleur rouge à la colère. Prenons un autre exemple qui est la **joie** où c'est des formes plus rondes qui se forment. Sur les photos ci-dessous, il y a tellement d'états de joie que les formes se superposent et créent une couleur plus foncée.

![forme de la joie](images/joie.png) ![forme de la joie](images/joie2.png)

En lien avec la définition de l'**Inspiration** sur Littré, quel genre d'émotion m'entraîne en regardant les créations de Cesare Leonardi et Franci Stagi?
Elles m'ont inspiré à travailler les courbes et la notion d'équilibre, et comme la courbe est associée au sentiment de la joie, j'aimerais faire un objet construit essentiellement par des courbes qui évoquerait en théorie un sentiment de joie.

Suite à l'exercice précédant du module 4, j'aimerais aussi continuer de travailler cette impression de flotter.

![croquis](images/croquis2.png)

À partir des créations les plus connues de Cesare Leonardi et Franca Stagi, j'ai essayé de bien analyser chaque oeuvre pour voir quelle forme m'inspire. Plusieurs dessins m'ont amené à faire des formes courbées avec des éléments flottants.

Cette modélisation m'a permis de jouer avec les formes courbées.
![premier essai de modélisation](images/test2.png)

Cette modélisation me permettra de voir la manière dont la table se met en équilibre, ayant la zone de travail plus grande que la base.
![deuxième essai de modélisation](images/test3.png)

En faisant les modélisation, je n'étais pas assez satisfait du jeu des formes et de l'impression de flotter. J'ai voulu alors pousser les limites et je suis tombé sur ce concept très particulier qui s'appelle la **tenségrité**. La tenségrité est la faculté d'une structure à se stabiliser par le jeu des forces de tension et de compression qui s'y répartissent et s'y équilibrent. C'est ce concept qui va me permettre d'atteindre cette impression de flotter.

![tensegrity](images/tensegrity1.png)
![croquis tensegrity](images/croquis3.png)
![croquis tensegrity](images/croquis4.png)

J'ai tout d'abord essayé de modéliser un petit exemple sur **Fusion 360**. Les liens de tension et compression seront fait par des fils pour commencer.

![fusion 360 model](images/tensegrity_fusion.png)

et voici le modèle importé sur prusa, y compris l'objet de référence.

![prusa tenségrité](images/prusa_table.png)

Une fois les pièces imprimées, je les ai assemblées grâce à des fils. C'était très difficile car il fallait que les trois longues fils aient la même taille mais cela n'est pas évident sur une petite échelle.
![prototype tenségrité](images/prototype1.png)

Les parties diagonales des pièces se sont aussi mal imprimées alors que j'ai généré des supports pour cela sur Prusa.
![prototype tenségrité](images/prototype3.png)
![prototype tenségrité](images/prototype4.png)

Comme vous voyez, la pièce supérieure n'est pas horizontale. C'est parce que le fil entouré en rouge n'a pas la même longueur que les deux autres. Cependant, les trois fils sont assez tendus pour que la pièce supérieure tienne.

![prototype tenségrité](images/prototype2.png)

En conclusion, l'expérimentation est réussie. Le modèle imprimé en 3D tient et peut supporter certains objets mais tout dépend de son poids. Il suffit aussi qu'il y ait une force horizontale qui est exercée au modèle que la pièce supérieure tombe. Pareil, lorsqu'on applique une force verticale relativement forte sur un coin de la pièce supérieure, celle-ci peut facilement basculer.

**Étape suivante**

Pour cette étape, je vais essayer de continuer sur une imprimante 3D pour l'instant mais sur une échelle plus grande. Je travaillerai ensuite avec la découpe laser pour faire une table basse à une échelle réelle. 
Je vais aussi essayer de travailler sur plus de formes courbées comme montré ci-dessous.
Ensuite, j'ai eu l'idée de faire les liens de tension et compression par des chaînes qui seront imprimées avec l'imprimante 3D. On trouvera sur les bouts des chaînes un système de filetage qui va me permettre d'attacher les chaînes sur les deux bases. Les trous sur les bases seront alors remplacés par un système de filetage. Cela me permettra d'avoir les même longueurs d'attache et éviter le problème rencontré à l'étape d'expérimentation.

![croquis projet](images/croquis5.png)

Après quelques reflexions, j'ai abandonné l'idée d'utiliser des chaînes pour faire le lien entre les deux pièces principales et j'ai décidé d'utiliser tout simplement du nylon car j'aimerais garder cette légereté que le fil de nylon peut donner contrairement à la chaîne qui reste très présente dans la structure. Le nylon possède aussi des facteurs élastiques qui va me permettre d'étendre facilement les fils avec le nouveau système de filetage et boulon que j'ai mis en place.

![système de filetage](images/finalproject3.png)
![système de filetage](images/finalproject4.png)

Dans l'ensemble des deux pièces principales, j'ai essayé d'enlever de la matière et j'ai gardé les formes courbées. Pour la partie inférieure, je me suis inspiré du pli de mon object de référence qui sert de rangement pour les magasines ou livres. Enlever de la matière me permettra d'alléger les pièce et donner cette impression de légereté que je cherche.

![référence du pli](images/finalproject5.png)

Voici les différentes pièces de mon projet:

![projet final de son ensemble vue 1](images/finalproject1.png)
![projet final de son ensemble vue 2](images/finalproject2.png)

et voici le résultat final en rendu sur Fusion 360:

![rendu fusion 360](images/render_pre-jury_project.png)

Les traitillés jaunes représentent les fils de nylon qui serviront de liens entre les deux bases triangulaires et qui seront ajoutés lors de l'assemblage.

![croquis de détails de la partie inférieure](images/croquis6.png)
![croquis de détails de la partie supérieure](images/croquis7.png)

Voici le tutoriel de l'assemblage

![croquis de tutuoriel de l'assemblage](images/croquis8.png)

**Texte de présentation**

Mon projet est une petite table basse qui se nomme "**TAFL**" qui est un jeu de mots avec la traduction néerlandaise du mot "table" mais qui est aussi une fusion des deux mots **TA**ble **FL**ottante. L'**inspiration** que j'ai eu pour faire cet objet vient de la _Kappa_ de Cesare Leonardi et Franca Stagi qui est une table basse en fibre de verre possédant une sorte de pli concave où l’on peut ranger des magazines ou des livres. La _Kappa_ m'a inspiré de faire un objet avec des jeu de formes courbées qui traduit la légereté et m'a inspiré d'appliquer cette impression de flotter sur mon objet qu'on retrouve d'ailleurs sur les autres oeuvres de Leonardi et Stagi. 

Le projet est composé de deux grandes bases triangulaires (une supérieure et une inférieure) liées par des fils. Les fils exercent une force de tension et les forces de compression se répartissent sur tout l’objet. Le fait que la pièce supérieure du projet tient que sur des fils donne cette impression de flottement. Le projet en général présente des formes triangulaires et des formes courbées qui donne une certaine légèreté. Les fils sont atttachés par un système de filetage et de boulon pour pouvoir étirer les fils et exercer plus de tension.

**Préparation pour le jury final**

Lors de mon **pré-jury**, j'ai présenté le principe de tenségrité dont les professeurs n'étaient pas très convaincu. Le principe ne rejoignait pas l'objet de référence, ni le jeu de profil. Ce qui les a le plus attiré l'attention est un de mes objets 3D d'expérimentation:

![deuxième essai de modélisation](images/test3.png)

L'objet imprimé était le résultat d'une erreur, j'avais commencé l'impression avec très peu de fil. J'avais du donc changer la couleur du fil quelques heures après l'impression. Le profil de l'objet était mis en valeur par le fait qu'il était vert et le reste de l'objet était noir.

![photo de l'objet imprimé](images/prototype9.png)

J'ai alors refait **l'exercice de dessin de profil** pour générer un nouveau que j'utiliserai pour mon projet final.
![travail de recherche par des petits croquis](images/croquis9.png)

J'ai tout d'abord essayé de reproduire le profil que j'ai choisi sur **Fusion 360** pour pouvoir l'imprimer plus tard et voir comment ça ressemblera sur une plus petite échelle. La petite maquette me permettra aussi de voir si l'objet est assez stable.

![travail de recherche par des petits croquis](images/croquis10.png)

À travers des croquis, j'ai essayé de voir comment j'allais construire l'objet à l'échelle 1/2 grâce à la Shaper. Si vous voulez apprendre comment utiliser la Shaper, voici [le lien du tutoriel](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero/machines/-/blob/master/Shaper.md). 

J'ai tout d'abord pensé à découper le même profil plusieurs fois mais cela ferait énormément de pertes de matériau, de temps de travail et l'objet final sera très lourd. J'ai alors décidé réduire de la matière pour alléger la table basse. Pour la première maquette, j'ai décidé de faire la découpe sur un [panneau mdf](https://www.brico.be/fr/construction/bois/panneaux-mdf/panneau-mdf-sencys-haute-densit%C3%A9-122x61cm-12mm/1887980). J'ai ensuite assemblé les pièces avec de la colle à bois. Ce travail réalisé semblait très lourd visuellement et aussi au niveau du matériau, et perdait aussi l'aspect de la légèreté de mon objet de référence. -> FAIL

![expérimentation avec la shaper](images/shapertry.png)

**Design final**

Avant d'arriver à mon design final, j'ai tenté plusieurs expérimentations avec le profil de référence et différentes compositions. J'ai d'abord fait plusieurs croquis pour les visualiser et ensuite je suis passé à l'imprimante 3D pour avoir des maquettes à l'échelle 1:100.

![expérimentation](images/prototype10.png)

![croquis de recherche](images/croquis11.png)

Après plusieurs réflexions à l'aide de mes collègues et mes professeurs, je suis amené à trouver un profil et une composition de pièces plus légère comme montré sur la photo ci-dessous. Je trouvais que cette forme était la plus simple et la plus facile à réaliser à l'échelle réelle. On retrouve l'aspect de l'objet de référence, la _KAPPA_, qui semble être composé à partir d'une longue feuille pliée dans tous les senses pour obtenir sa forme finale. On retrouve aussi la légèreté et la simplicité.


L'objet final sera une table d'appoint que l'on peut installer à côté du canapé et sur laquelle on peut poser des livres, son ordinateur portable et son café par exemple.

**Dimensions**

![dimensions de l'objet final](images/dimensions.png)

**Conception à l'échelle réelle**

Au début, je voulais faire tout l'objet en lamelle mais cela ferait une très grosse perte de matériau, de temps et d'argent! C'est pourquoi, pour le profil, j'ai décidé de travailler en lamelle pour réaliser les courbes plus facilement. Pour les autres parties, j'ai décidé d'utiliser une planche pour éviter les pertes. Je découpe chaque morceau avec la Shaper Origin et j'assemblerai toutes les pièces avec de la colle et des visses.

Pour la question du matériau, un panneau de multiplex d'1,8cm sera utilisé pour un profil entier dans le but de mettre en évidence le profil de la table. Le reste sera en MDF d'1,8cm qui sera peint en noir pour avoir un contraste.

![croquis de recherche](images/croquis12.png)

![projet final](images/design_finaaaal.png)


**Coûts de production**

![comparaison de coût](images/côûts.png)

* Colle à bois : 7€
* 4x Planche MDF 122x61x18 : 53,95€
* Visses : fournies par l'atelier
* Peinture noire : 19,99€ 




