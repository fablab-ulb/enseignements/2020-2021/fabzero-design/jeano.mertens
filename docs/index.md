Hey! Bienvenue!


## À propos de moi

![portrait](images/photositeweb.jpg)

Bonjour! Je m'appelle Jeano Mertens :v:. Je suis né aux Philippines et je suis venu en Belgique à l'âge de 10 ans. 
J'aime beaucoup la musique :notes: , je joue à la guitare et au ukulele. Je chante aussi et je postais des _covers_ sur _YouTube_. Je danse aussi depuis tout petit et je participais à des compétitions aux Philippines.
J'ai toujours aimé bricoler depuis tout petit. Cela a commencé avec les _LEGO_ et ensuite Les _Sims_. C'est peut-être grâce à ceux-ci que je fais des études d'architecture.
J'ai fait mes trois ans de bachelier à l'ULB La Cambre Horta en Architecture et je fais en ce moment mon deuxième année de master. Je suis les cours de Logement Innovant en projet et Architecture&Design en option, à travers lequel je développe mes connaissances dans le numérique dont ce site blog.

Voici mon [portfolio](https://jeanobmertens.wixsite.com/jxmdesign) pour constater mes travaux en bachelier. Ce portfolio n'est pas officiel puisqu'il s'agissait d'un travail pour un cours en MA1.


## Premier exercice

Nous avons visité l'**ADAM - Design Museum Brussles** le 1er octobre 2020 pour choisir un objet qu'on a décidé de travailler avec.

J'ai choisi la _KAPPA_ de **Cesare Leonardi et Franca Stagi** (71cm x 76cm x 26cm). C'est une table  basse datant de 1970. Elle est faite en fibre de verre laqué et a la couleur de blanc crème. Elle possède une partie concave produite par un pli, où l'on peut mettre des magasines ou des livres. La stabilité de la table est assurée par la base en triangle grâce au pli. 

![kappa](images/1kappa.png)

Je l'ai choisi comme mon deuxième objet car j'aimais le fait que la table ne se tenait pas sur quatre pieds comme une table ordinaire et aussi le fait qu'il y a un espace consacré pour ranger des objets. Le design fait aussi que plusieurs tables de même type peuvent s'emboîter, comme des chaises.

![kappa photos d'expo](images/3kappa.png)

